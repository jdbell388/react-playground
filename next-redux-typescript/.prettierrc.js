module.exports = {
  singleQuote: true,
  trailingComma: 'es5',
  printWidth: 100,
  bracketSameLine: true,
  arrowParens: 'avoid',
  quoteProps: "as-needed",
};
