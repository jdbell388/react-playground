/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['marvelcdb.com'],
  }
}

module.exports = nextConfig
