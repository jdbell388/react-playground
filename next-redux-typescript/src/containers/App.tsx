import { ServerResponse } from 'http';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Image from 'next/image';

import { requestCards, setSearchField } from '../actions';

const mapStateToProps = (state: any) => {
  return {
    searchField: state.searchCards.searchField,
    cards: state.requestCards.cards,
    isPending: state.requestCards.isPending,
    error: state.requestCards.error,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onSearchChange: (event: any) => dispatch(setSearchField(event.target.value)),
    onRequestCards: () => dispatch(requestCards()),
  };
};

function App(props: any) {
  const { searchField, onSearchChange, onRequestCards, cards, isPending } = props;

  const filteredCards =
    cards &&
    cards.length > 0 &&
    cards.filter((card: any) => {
      return card.name.toLowerCase().includes(searchField.toLowerCase());
    });

  useEffect(() => {
    onRequestCards();
  }, []);

  return (
    <div>
      <h2>Marvel Champions Card Finder</h2>

      <label htmlFor="search">Search</label>
      <input
        type="text"
        onChange={(event: any) => onSearchChange(event)}
        name="search"
        value={searchField}
      />
      <p>{searchField && searchField}</p>
      {!isPending && filteredCards ? (
        filteredCards.map((card: any) => (
          <div key={card.code}>
            <h3>{card.name}</h3>
            {card.imagesrc && (
              <Image
                src={`https://marvelcdb.com${card.imagesrc}`}
                alt={card.name}
                width={300}
                height={419}
              />
            )}
          </div>
        ))
      ) : (
        <h3>Loading...</h3>
      )}
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
