import {
  CHANGE_SEARCHFIELD,
  REQUEST_CARDS_PENDING,
  REQUEST_CARDS_FAILED,
  REQUEST_CARDS_SUCCESS,
} from './constants';

export const setSearchField = (text: string) => ({
  type: CHANGE_SEARCHFIELD,
  payload: text,
});

export const requestCards = () => (dispatch: any) => {
  dispatch({ type: REQUEST_CARDS_PENDING });

  fetch('https://marvelcdb.com/api/public/cards/')
    .then(response => response.json())
    .then((cards: any) => {
      console.log('allcards', cards);
      dispatch({ type: REQUEST_CARDS_SUCCESS, payload: cards });
    })
    .catch((error: any) => dispatch({ type: REQUEST_CARDS_FAILED, payload: error }));
};
