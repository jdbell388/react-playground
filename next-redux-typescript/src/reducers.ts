import {
  CHANGE_SEARCHFIELD,
  REQUEST_CARDS_PENDING,
  REQUEST_CARDS_FAILED,
  REQUEST_CARDS_SUCCESS,
} from './constants';

const initialStateSearch = {
  searchField: '',
};

export const searchCards = (state: any = initialStateSearch, action: any = {}) => {
  switch (action.type) {
    case CHANGE_SEARCHFIELD:
      return Object.assign({}, state, { searchField: action.payload });
    default:
      return state;
  }
};

const initialStateCards = {
  isPending: false,
  robots: [],
  error: '',
};

export const requestCards = (state: any = initialStateCards, action: any = {}) => {
  switch (action.type) {
    case REQUEST_CARDS_PENDING:
      return Object.assign({}, state, { isPending: true });
    case REQUEST_CARDS_SUCCESS:
      return Object.assign({}, state, { cards: action.payload, isPending: false });
    case REQUEST_CARDS_FAILED:
      return Object.assign({}, state, { error: action.payload, isPending: false });
    default:
      return state;
  }
};
